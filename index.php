<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="jquery-1.10.2.min.js"></script>
<script src="jquery.form.js"></script>
<script src="jquery-cookie-1.3.1/jquery.cookie.js"></script>
<script src="jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Circos Generator</title>
<link href="rcircos.css" rel="stylesheet" type="text/css" />
<script type="text/css">
</script>
<script type="text/javascript">
function cookieAjax() {
	return 		$.ajax({
		type: "POST",
		url: "userSearch.php",
		data: { key: get_cookie('rcircostoken')},
	});
}
$(document).ready(function() {
	$('#tabs').tabs();
	var optionsinit = {
		    target:        '#seed_div',   // target element to be updated with server response 
		    url: 'fileupload.php',
		}; 
	// check if the user has a cookie and if they do use it as the ID
	if(get_cookie('rcircostoken') != null) {
		var promise = cookieAjax();
		promise.success(function (data) {
			$('#seed_div').html(data);
			if(data.substring(0, 9) == "<script>v") {
				$('#start').css('display', 'inline');
				$('#deleteFiles').css('display', 'inline');
		    	$('#idForm').ajaxSubmit(optionsinit);
			}
		});
	}
	var deloptions = {
			target:			'#delete_div',
			url: 'delFiles.php',
	};
	/* when the user clicks the start button it hides the data upload form 
		and displays the plot making form */
	$('#start').click(function() {
		$('#idForm').ajaxSubmit(optionsinit);
		$('#tabs').tabs('option', 'active', 1);
//		$('#idForm').css('display','none');
//		$('#circosForm').css('display', 'block');
	});
	$('#tab2').click(function() {
		$('#idForm').ajaxSubmit(optionsinit);
	});
    /* when the user clicks the data upload button the form is submitted to
    	a file which creates the appropriate files in the directory
    	and creates the table showing the uploaded files */
	$('#idForm').submit(function() { 
		$('#start').css('display', 'inline');
		$('#deleteFiles').css('display', 'inline');
		$('#circosForm').css('display', 'block');
		$('#preID').css('display', 'none');
	    $(this).ajaxSubmit(optionsinit); 
		var control = $("#file1");
		control.val("");
	    control.replaceWith( control = control.clone( true ) );
		var control2 = $("#file2");
		control2.val("");
	    control2.replaceWith( control2 = control2.clone( true ) );
	    document.getElementById('track2row').style.display='none';
		var control3 = $("#file3");
		control3.val("");
	    control3.replaceWith( control3 = control3.clone( true ) );
	    document.getElementById('track3row').style.display='none';
		var control4 = $("#file4");
		control4.val("");
	    control4.replaceWith( control4 = control4.clone( true ) );
	    document.getElementById('track4row').style.display='none';
		var control5 = $("#file5");
		control5.val("");
	    control5.replaceWith( control5 = control5.clone( true ) );
	    document.getElementById('track5row').style.display='none';
		var control6 = $("#file6");
		control6.val("");
	    control6.replaceWith( control6 = control6.clone( true ) );
	    document.getElementById('track6row').style.display='none';
		var control7 = $("#file7");
		control7.val("");
	    control7.replaceWith( control7 = control7.clone( true ) );
	    document.getElementById('track7row').style.display='none';
	    return false;
	});
    var options = { 
        target:        '#circos_div',   // target element to be updated with server response 
    }; 
    /* when the user clicks the start button on the second form the data is 
    	submitted to a file which creates the plot and displays it underneath
    	the form. While it is processing a spinner appears */
    $('#circosForm').submit(function() {
    	document.getElementById('circos_div').innerHTML = '<img src=\'images/spinner2-greenie.gif\'   style=\'position:relative; left:0px;\' height=\'30\' width=\'30\' />';
        $(this).ajaxSubmit(options); 
        $('#modal').dialog({ dialogClass: "no-close", modal: true});
        $('#modal').dialog( "open" );
        window.setTimeout(function() {
        	$('#modal').dialog('close');
        	$('#modal2').dialog( {modal:true});
        	$('#modal2').dialog('open');
        }, 5000);
        return false; 
    });
}); 
</script>
<script type="text/javascript">
/* a function to make handling cookies easier. It takes cookie info
 * and then creates a cookie based on that if it is valid as well as
 * converting the usual seconds into days
 */
function set_cookie ( cookie_name, cookie_value, lifespan_in_days) {
	$.cookie(cookie_name, cookie_value, {expires: lifespan_in_days});
}
/* a function to make handling cookies easier. It takes cookie info 
 * and returns any information stored as a cookie that matches that
 * info. If the cookies does not exits it returns an empty string 
 */
function get_cookie ( cookie_name )
{
	return $.cookie(cookie_name);
}
/* a function to make handling cookies easier. It takes cookie info
 * and if that cookie exists it deletes it
 */
function delete_cookie ( cookie_name )
{
	$.removeCookie(cookie_name);
}
/* a function that takes the number of the track that needs to be changed
 * and then changes the download sample data link to the file that matches
 * the selected type of track.
 */
function trackDownload(num) {
	var trackType = 'trackType' + num;
	var trackA = "track" + num +"_a";
	var doc = document.getElementById(trackType);
	var type = doc.options[doc.selectedIndex].value;
	document.getElementById(trackA).onclick = function(){ return popitup("rcircos_help.html#" + type); } ;
}
/* This function makes sure that the entered track height does not exceed 3
 * since the plot does not come out correctly if it does
 */
function fixHeight() {
	trackHeight = document.getElementById('trackHeight');
	if(trackHeight.value > 3) {
		trackHeight.value = 3;
		alert("Warning! Track Height cannot be greater than 3!");
	}
}
/* This function ensures that no illegal characters will be included in the
 * plot name to prevent errors
 */
function fixName() {
	var illegal = new Array(':', '|','/', '\\', '(', ')', '.', '~', "'", '"', "?", "#", "$", "%", "&", ",", "!", "@", "^", "*", ";", "[", "]", "{", "}", "<",">","+", "-", " ", "=");
	var name = $('#plotName').val();
  	var error = false;
  	length = illegal.length;
	for(var i = 0; i<length; i++) {
		var charExists = (name.indexOf(illegal[i]) >= 0);
		if(charExists) {
			error = true;
			var character = illegal[i];
			var rnum = name.split(character);
			var len = rnum.length;
			name = '';
			for(var c = 0; c<len; c++) {
				name += rnum[c];
			}
		}
	}
	if(error) {
		$('#plotName').val(name);
		window.alert('Illegal characters have been removed from the plot name');
	}
}
/* This function creates the pop up windows that the help information shows up in
 * and shifts the focus to the new window.
 */
function popitup(url) {
	newwindow=window.open(url, 'name', 'height=600, width=800, scrollbars=yes');
	if(window.focus) {newwindow.focus()}
	return false;
}
/* HTML's reset function does not work properly with some elements of the form
 * so this manually resets the visability of chromosomes and download data links
 */
function speciesReset() {
	document.getElementById('track3_div').innerHTML = "<a href='Sampledata/exampleLineData.txt'>Download Data</a>";
	document.getElementById('Chr20').style.display = "table-cell";
	document.getElementById('Chr21').style.display = "table-cell";
	document.getElementById('Chr22').style.display = "table-cell";
	document.getElementById('ChrY').style.display = "table-cell";
	document.getElementById('track1_div').innerHTML = "<a href='Sampledata/exampleHeatmapData.txt'>Download Data</a>";
	document.getElementById('track2_div').innerHTML = "<a href='Sampledata/exampleHistogramData.txt'>Download Data</a>";
	document.getElementById('track4_div').innerHTML = "<a href='Sampledata/exampleScatterData.txt'>Download Data</a>";
	document.getElementById('track5_div').innerHTML = "<a href='Sampledata/exampleTileData.txt'>Download Data</a>";
}
/* Since each species has different chromosomes the chromosome exclude table 
 * must be modified to acurately reflect the chromosomes that the species has.
 * This function simply hides or shows the ones that are supposed to be there. 
 */
function changeSpecies() {
	var speciesName = document.getElementById('speciesName');
	if(speciesName.value == "Human") {
		document.getElementById('Chr20').style.display = "table-cell";
		document.getElementById('Chr21').style.display = "table-cell";
		document.getElementById('Chr22').style.display = "table-cell";
		document.getElementById('ChrY').style.display = "table-cell";
	} else if(speciesName.value == "Mouse") {
		document.getElementById('Chr20').style.display = "none";
		document.getElementById('Chr21').style.display = "none";
		document.getElementById('Chr22').style.display = "none";
		document.getElementById('ChrY').style.display = "table-cell";
	} else {
		document.getElementById('Chr20').style.display = "table-cell";
		document.getElementById('Chr21').style.display = "none";
		document.getElementById('Chr22').style.display = "none";
		document.getElementById('ChrY').style.display = "none";
	}
}
/* Generates a random 30 character long string that serves as the identifier 
 * for each unique user. The server creates a directory for this id and then
 * displays the id in the textbox and gives the option to save the id as a cookie
 */
function startNewSession() { 
	delete_cookie('rcircostoken');
	$('#fileManager').css('display', 'none');
	$('#step2').css('display', 'none');
	$('#step3').css('display', 'none');
	var chars = "1234567890abcdefghijklmnopqrstuvwxyz";
	var token = "";
	var c = 0;
	while(c <= 30) {
		token = token.concat(chars.charAt(Math.random()*40));
		c++;
	}
	$.ajax({
		type: "POST",
		url: "makeDirectory.php",
		data: { username: token}
	}).done(function() {
		var dis = document.getElementById('userID');
		dis.value = token;
		$('#username').val(token);
		dis.type = "text";
		$("#ID").html('');
		$('#username').val(token);
		$("#cookie").html("<button type='button' onclick='Javascript: saveCookie();'>Save as Cookie</button>");
	});

}
function saveCookieAjax() {
	return 		$.ajax({
		type: "POST",
		url: "hash.php",
		data: { plaintext : document.getElementById('userID').value},
	});
}
/* saves the id in the textbox as a cookie that will by default apear whenever
 * the user returns to the site and takes away the textbox for id input.
 */
function saveCookie() {
	var promise = saveCookieAjax();
	promise.success(function (data) {
		$('#circos_div').html(data);
	});
//	var dis = document.getElementById('userID');
//	dis.type = "hidden";
//	$("#ID").html(dis.value);
//	set_cookie('rcircostoken', dis.value, 14);
//	$("#cookie").html("<button type='button' onclick='Javascript: forgetCookie();'>Forget Cookie</button>");
}
/* 
 * deletes the user's cookie and returns the function to input a user provided id 
 */
function forgetCookie() {
	var dis = document.getElementById('userID');
	dis.type = "text";
	dis.value = $('#ID').html();
	$("#ID").html('');
	delete_cookie('rcircostoken');
	$("#cookie").html("<button type='button' onclick='Javascript: saveCookie();'>Save as Cookie</button>");
}
/* 
 * Switches the display to the currently selected plot from the hitory menu
 */
function histUpdate() {
	var username = $('#userID').val();
	var circosPlot = $('#historySelect').val() ;
	if(circosPlot.slice(-3) == 'pdf') {
		$('#circos_div').html("<a href='" + circosPlot + "'>Download Plot</a>");
	} else {
		$('#circos_div').html("<img align='center' src='" + circosPlot +"' height='800' width='800' >");
	}
}
/*
 * Draws a curved given start and stop coordinates
 */
function DrawLine(x1, y1, x2, y2, color) {
	var canvas = document.getElementById('relationsCanvas');
	var context = canvas.getContext('2d');
	var posAdjust = $('#relationsCanvas').offset();
	x1 = x1-posAdjust.left;
	y1 = y1-posAdjust.top;
	x2 = x2-posAdjust.left;
	y2 = y2-posAdjust.top;
	context.beginPath();
	context.lineWidth = 5;
	context.moveTo(x1, y1);
	context.quadraticCurveTo(200 , y2 , x2, y2);
    context.strokeStyle = color;
	context.stroke(); 
	}
/*
 * Espaces characters that need to be escaped in a jquery ID selector
 */
function jq( myid ) {
	return myid.replace( /(:|\.|\[|\])/g, "\\$1" );
}
/*
 * Check/uncheck all function
 */
function toggleCheckboxes() {
	flag = $('#toggle:checked').val();
    var form = document.getElementById('idForm');
    var inputs = form.elements;
    if(!inputs){
        //console.log("no inputs found");
        return;
    }
    if(!inputs.length){
        //console.log("only one elements, forcing into an array");
        inputs = new Array(inputs);        
    }
    for (var i = 0; i < inputs.length; i++) {
      //console.log("checking input");
      if (inputs[i].type == "checkbox" & inputs[i].id != 'toggle') {  
        inputs[i].checked = flag;
      }
    }
}
</script>
</head>
<body>
<!--[IF IE]>
    <script type="text/javascript">
           <script>window.alert('WARNING Internet explorer does not support many of the features on this website, please consider upgrading your browser');</script>
    </script>
<![endif]-->
<div id='delete_div'></div>
<div align="center">
<h1> Circos Plot Generator</h1> A web interface for <a href="http://cran.r-project.org/web/packages/RCircos/index.html" target="_blank">the RCircos r package</a><br><strong></div>
<div id="user_div" name="user_div" align=center></div>
<table align='center'><tr><td>
<div id='tabs'>
<ul>
    <li><a href="#idForm_div" onclick="Javascript: optionsinit = {target: '#seed_div', url: 'fileupload.php' };  $('#idForm').ajaxSubmit(optionsinit); "><span>File Manager</span></a></li>
    <li><a id='tab2' href="#generator"><span>RCircos</span></a></li>
    <li><a href="#workflow"><span>Workflow</span></a></li>
    <li><a href="#help"><span>File Formatting</span></a></li>
</ul>
<div id='workflow'><br>
<table align="center"><tr align="left"><td>
<h2>Step by step instructions</h2>
<p><br>
<p><strong>Step 1: Create Your Data</strong><br>Refer to the <a onmouseover="Javascript: document.getElementById('formatlink').color='#FF0000';" onmouseout="Javascript: document.getElementById('formatlink').color='#0000FF';" onclick="Javascript: $('#tabs').tabs('option', 'active', 3);" target="_blank"><font id='formatlink' color='#0000FF'>formatting</font></a> page to see references to base your files off of and ensure that files match exactly the format presented <br> in the examples or the program might be unable to recognize how to use it. </p>
<p><strong>Step 2: Upload Your Data</strong><br>Once you are confident that the data is formatted correctly load the <a onmouseover="Javascript: document.getElementById('mainlink').color='#FF0000';" onmouseout="Javascript: document.getElementById('mainlink').color='#0000FF';" onclick="Javascript: $('#tabs').tabs('option', 'active', 0);" target="_blank"><font id='mainlink' color='0000FF'>file manager</font></a> and click the start new session button. If will <br> generate a filemanager that will handle all the files you upload and create and display the ID you will use to acces this account. <br>
You now have the choice of either saving the ID as an encrypted cookie in your browser or keeping it stored somewhere else. <br> When you return to the site the ID will automatically be loaded for you, if there is no cookie it will require you to enter the one <br> you recieved when creating the session. Now select the files you want to upload and simply click the upload data button.
<br></p><h5>WARNING: Several non numeric or alphabetic characters interfear with scripts running on this page. To make the process as smooth as <br>
possible these characters will be removed from the file name and replaced with a capital R. If the replacement filename already exists the<br> old file will be overwritten</h5>
<p><strong>Step 3: Select the Data You Want to Use</strong><br>If your data was formatted correctly it will automattically sort the files out and only give them as options for track types that <br> match their formatting. Under each track type there are links to formatting advice and to download <strong>(All downloads require a  <br>right click and selecting 'save link as')</strong> sample data. Once you have selected the data you want and made the specifications <br> necessary simply press start and it will begin making the plot.</p>
</td></tr></table>
</div>
<div id='help'>
<br>
<table align="center"><tr align="left"><td>
<h2>Data Format Examples for RCircos Web Application</h2>
<p><br>
  <h3>!!! Important !!! All data uploaded should be in tab delimited or comma separated format.<br> If the plot is not displaying correctly please make sure that the data <br> is formatted similar to the examples below</h3>
  <br>
  <a name="email" id="email"></a>
  <p><strong>Why do we request your email address?</strong> <br><br>
  Providing your email address allows us to send you an email when your plot <br>
  has finished. This is especially helpful for large data sets which may take <br>
  a few minutes to complete. You may provide multiple emails separated by <br>
  commas but be careful who you send these links to. The links contain your <br>
  user ID and will allow anyone who has it to access your data. <br><br>
  Emails are never saved on the server and cannot be used for any other purpose</p>
  <br>
  The first three columns of the data table, except for input to the link <br>
  plot, must be genomic position information in the order of chromosome names, <br>
  chromosome start, and chromosome end positions. <br> 
  <br>
  For gene labels and heatmap plots, the gene/probe names must be provided <br>
  in the fourth column. For other plots, this column could be optional. <br>
<p>Test Data: <a href="rcircos_test_data.zip">Download</a></p>
<a name="geneLabels" id="geneLabels"></a>
<p><strong>Gene Label Data</strong><br>
</p>
<table cellspacing="3" cellpadding="3">
  
  <tr>
    <td>Chromosome </td>
    <td>chromStart</td>
    <td>chromEnd</td>
    <td>Gene</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >8921418</td>
    <td >8934967</td>
    <td>ENO1</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >17345375</td>
    <td >17380514</td>
    <td>SDHB</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >27022894</td>
    <td >27107247</td>
    <td>ARID1A</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >41976121</td>
    <td >42501596</td>
    <td>HIVEP3</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >43803519</td>
    <td >43818443</td>
    <td>MPL</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >45794977</td>
    <td >45805926</td>
    <td>MUTYH</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >65300244</td>
    <td >65351947</td>
    <td>JAK1</td>
  </tr>
</table>
<br>
<a name="Heatmap" id="Heatmap"></a>
<strong>Heatmap Data</strong> <br>
<table cellspacing="3" cellpadding="3">
  <col >
  <col>
  <col>
  <col >
  <col>
  <col>
  <col width="64">
  <col width="56" span="3">
  <tr>
    <td >Chromosome</td>
    <td>chromStart</td>
    <td>chromEnd</td>
    <td >GeneName</td>
    <td>X786.O</td>
    <td>A498</td>
    <td width="64">A549.ATCC</td>
    <td width="56">ACHN</td>
    <td width="56">BT.549</td>
    <td width="56">CAKI.1</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >934341</td>
    <td >935552</td>
    <td>HES4</td>
    <td >6.75781</td>
    <td >7.38773</td>
    <td >6.4789</td>
    <td >6.05517</td>
    <td >8.85062</td>
    <td >7.00307</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >948846</td>
    <td >949919</td>
    <td>ISG15</td>
    <td >7.56297</td>
    <td >10.4959</td>
    <td >5.89893</td>
    <td >7.58095</td>
    <td >12.0847</td>
    <td >7.81459</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >1138887</td>
    <td >1142089</td>
    <td>TNFRSF18</td>
    <td >4.69775</td>
    <td >4.55593</td>
    <td >4.3897</td>
    <td >4.50064</td>
    <td >4.47525</td>
    <td >4.47721</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >1270657</td>
    <td >1284492</td>
    <td>DVL1</td>
    <td >7.76886</td>
    <td >7.52194</td>
    <td >6.87125</td>
    <td >7.03517</td>
    <td >7.65386</td>
    <td >7.69733</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >1288070</td>
    <td >1293915</td>
    <td>MXRA8</td>
    <td >4.49805</td>
    <td >4.72032</td>
    <td >4.62207</td>
    <td >4.58575</td>
    <td >5.66389</td>
    <td >4.93499</td>
  </tr>
</table>
<br>
<a name="Histogram" id="Histogram"></a>
<strong>Histogram Data</strong> <br>
<table cellspacing="3" cellpadding="3">
  <tr>
    <td >Chromosome</td>
    <td>chromStart</td>
    <td colspan="2" width="167">chromEndData</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >45000000</td>
    <td >49999999</td>
    <td >0.070859</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >55000000</td>
    <td >59999999</td>
    <td >0.30046</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >60000000</td>
    <td >64999999</td>
    <td >0.125421</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >70000000</td>
    <td >74999999</td>
    <td >0.158156</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >75000000</td>
    <td >79999999</td>
    <td >0.16354</td>
  </tr>
</table>
<br>
<br>
<a name="Line" id="Line"></a>
<strong>Line Plot Data</strong> <br>
<table cellspacing="3" cellpadding="3">
  <tr>
    <td >chromosome</td>
    <td>start</td>
    <td>stop</td>
    <td >num.mark</td>
    <td>seg.mean</td>
  </tr>
  <tr>
    <td >1</td>
    <td >61735</td>
    <td >16895627</td>
    <td >8732</td>
    <td >0.1797</td>
  </tr>
  <tr>
    <td >1</td>
    <td >16896821</td>
    <td >17212714</td>
    <td >105</td>
    <td >-0.2117</td>
  </tr>
  <tr>
    <td >1</td>
    <td >17214822</td>
    <td >25574471</td>
    <td >5321</td>
    <td >0.1751</td>
  </tr>
  <tr>
    <td >1</td>
    <td >25574707</td>
    <td >25662212</td>
    <td >37</td>
    <td >0.5064</td>
  </tr>
  <tr>
    <td >1</td>
    <td >25663310</td>
    <td >30741496</td>
    <td >2400</td>
    <td >0.1384</td>
  </tr>
  <tr>
    <td >1</td>
    <td >30741656</td>
    <td >30745210</td>
    <td >3</td>
    <td >-1.4742</td>
  </tr>
</table>
<br>
<br>
<a name="Scatter" id="Scatter"></a>
<strong>Scatter Plot Data</strong> <br>
<table cellspacing="3" cellpadding="3">

  <tr>
    <td >chromosome</td>
    <td>start</td>
    <td>stop</td>
    <td >num.mark</td>
    <td>seg.mean</td>
  </tr>
  <tr>
    <td >1</td>
    <td >61735</td>
    <td >228706</td>
    <td >18</td>
    <td >-0.4459</td>
  </tr>
  <tr>
    <td >1</td>
    <td >228729</td>
    <td >356443</td>
    <td >10</td>
    <td >0.5624</td>
  </tr>
  <tr>
    <td >1</td>
    <td >356542</td>
    <td >564621</td>
    <td >4</td>
    <td >-0.9035</td>
  </tr>
  <tr>
    <td >1</td>
    <td >603590</td>
    <td >1704138</td>
    <td >227</td>
    <td >0.3545</td>
  </tr>
  <tr>
    <td >1</td>
    <td >1709023</td>
    <td >1711414</td>
    <td >6</td>
    <td >1.2565</td>
  </tr>
  <tr>
    <td >1</td>
    <td >1714558</td>
    <td >12862252</td>
    <td >6276</td>
    <td >0.4027</td>
  </tr>
</table>
<br>
<br>
<a name="Tile" id="Tile"></a>
<strong>Tile Plot Data</strong> <br>
<table cellspacing="3" cellpadding="3">

  <tr>
    <td >Chromosome</td>
    <td>chromStart</td>
    <td>chromEnd</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >0</td>
    <td >23900000</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >12700000</td>
    <td >44100000</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >28000000</td>
    <td >68900000</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >59000000</td>
    <td >94700000</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >99700000</td>
    <td >120600000</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >147000000</td>
    <td >234700000</td>
  </tr>
</table>
<br>
<br>
<a name="linkLines" id="linkLines"></a>
<strong>Link Line Data</strong> <br>
<table cellspacing="3" cellpadding="3">
  <col >
  <col>
  <col>
  <col >
  <col>
  <col>
  <tr>
    <td >Chromosome</td>
    <td>chromStart</td>
    <td>chromEnd</td>
    <td >Chromosome.1</td>
    <td>chromStart.1</td>
    <td>chromEnd.1</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >8284703</td>
    <td >8285399</td>
    <td>chr1</td>
    <td >8285752</td>
    <td >8286389</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >85980143</td>
    <td >85980624</td>
    <td>chr7</td>
    <td >123161313</td>
    <td >123161687</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >118069850</td>
    <td >118070319</td>
    <td>chr1</td>
    <td >118070329</td>
    <td >118070689</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >167077258</td>
    <td >167077658</td>
    <td>chr1</td>
    <td >169764630</td>
    <td >169764965</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >171671272</td>
    <td >171671550</td>
    <td>chr1</td>
    <td >179790879</td>
    <td >179791292</td>
  </tr>
  <tr>
    <td>chr1</td>
    <td >174333479</td>
    <td >174333875</td>
    <td>chr6</td>
    <td >101861516</td>
    <td >101861840</td>
  </tr>
</table></td></tr></table>
<br>
<br></div>
<div id='idForm_div'><form id ="idForm" name="idForm" method="post" style="display: block" enctype="multipart/form-data">
<table width="700" align="center" cellpadding='3' cellspacing='2' style='border: 1px solid #CCCCCC;background-color:#DEEBDC; '>
  	  	<tr>
    	<td width="1">&nbsp;</td>
    	<td width="159">&nbsp;</td>
    	<td width="184"><button type=button id="session" onclick="Javascript: startNewSession(); var dis = document.getElementById('session'); dis.style.display = 'none';">Start New Session</button>
    	<INPUT type="Submit" Name=Submit VALUE="Submit session ID" id="mySubmit"></td>
    	<td width="131">&nbsp;</td>
    	<td width="7">&nbsp;</td>
  	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><div align="right">
				<strong>Your session ID:</strong>
			</div>
		</td>
		<td><input name="userID" type="text" id="userID" onchange="Javascript: $('#username').val($('#userID').val());"><div id="ID"></div></td>
		<td id="cookie"><button type='button' onclick='Javascript: saveCookie();'>Save as Cookie</button></td>
	</tr>

		<tr id='step2' style='display:none;'>
		  <td nowrap='nowrap'><strong>Files Uploaded:</strong></td><td colspan=8>&nbsp;</td></tr>
		<tr><td></td><td colspan=8><div id="seed_div"></div></td></tr>
        			<tr><td></td>
				<td nowrap='nowrap' align='right'><strong>Upload Files:</strong></td>
				<td colspan=6 align="center"><input type="file" name="file1" id="file1" onchange="Javascript: dis=document.getElementById('track2row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>
	 		<tr id="track2row" style="display: none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" value='' name="file2" id="file2" onchange="Javascript: dis=document.getElementById('track3row'); dis.style.display='table-row';">
				<td align="left"><INPUT type="Submit" Name=Submit VALUE="Upload" id="mySubmit"></td>
			</tr>	
	 	 	<tr id="track3row" style="display:none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" value='' name="file3" id="file3" onchange="Javascript: dis=document.getElementById('track4row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>	
			
			<!-- Track 4 data upload -->
			<tr id="track4row" style="display: none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file4" id="file4" onchange="Javascript: dis=document.getElementById('track5row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>
			<!-- Track 5 data upload -->
			<tr id="track5row" style="display: none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file5" id="file5" onchange="Javascript: dis=document.getElementById('track6row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>
			<tr id="track6row" style="display:none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file6" id="file6" onchange="Javascript: dis=document.getElementById('track7row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>
			<tr  id="track7row" style="display:none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file7" id="file7">
				<td>&nbsp;</td>
			</tr>
			<tr>
		<td colspan=6 align=center></td></tr>
		<tr id='step3' style='display:none;'>
		  <td><strong>Selected Files:</strong></td><td colspan=8>&nbsp;</td></tr>
		<tr> <td colspan=8 align=center>
		<button type='button' id='start' style="display: none">Go RCircos</button><button id="deleteFiles" type="button" style="display:none" onclick="Javascript: var answer = confirm('Are you sure you want to delete the selected files?'); if(answer){ $('#idForm').ajaxSubmit({url:'delFiles.php', type:'post', target: '#delete_div'});} return false;">Delete</button>
  	</td>
	</tr>
</table>
</form></div>
<div id='generator'>
<table id='preID'><tr><td align='center'>Looks like you haven't started a session! Starting a session allows you to keep files on the server and use them to create circos plots over and over again. Switch to the File Manager tab and get started!</td></tr></table>
<form id="circosForm" name="circosForm" method="post" action="rcircos_action.php" enctype="multipart/form-data" style="display: none">
<table width="800" align="center" cellpadding='3' cellspacing='2' style='border: 1px solid #CCCCCC;background-color:#DEEBDC; '>
  <tr>
    <td width="1"><input name="username" type="hidden" id="username"></td>
    <td width="159">&nbsp;</td>
    <td width="184">&nbsp;</td>
    <td width="80">&nbsp;</td>
    <td width="7">&nbsp;</td>
  </tr>
  <tr><td colspan=8 align="center"></td></tr>
 <!-- 
 Species data input
  -->
			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Species:</strong>
					</div></td>
				<td><select name='speciesName' id='speciesName' onchange="Javascript: changeSpecies()">
						<option value="Human" selected='selected'>Human (hg19)</option>
						<option value="Mouse">Mouse (mm9)</option>
						<option value="Rat">Rat (rn4)</option>
				</select></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
	<!-- 
	Gene label data upload
	 -->
				<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Gene Label Data:</strong><a href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#geneLabels')">?</a>
					</div></td>
				<td name="phGene">placeholder</td>
				<td nowrap="nowrap"><div align="left">
						<strong>Link Lines:</strong><a href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#tilePlots')">?</a>
					</div>
				</td>
				<td name="phLink">placeholder</td>
				</tr>
<!-- 
Track 1 data upload
 -->
 				<tr>
 				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 1:</strong>
					</div></td>
				<td nowrap="nowrap"><select name='trackType1' id='trackType1'  onchange="Javascript: trackDownload('1'); connectPlotType('1');">
						<option value="NONE" >No track</option>
						<option value="Heatmap" selected ='selected'>Heatmap</option>
						<option value="Histogram">Histogram</option>
						<option value="Line">Line Plot</option>
						<option value="Scatter">Scatter Plot</option>
						<option value="Tile">Tile Plot</option>
				</select>
				<a id='track1_a' href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#Heatmap')"> ? </a></td>
				<td name="ph1">placeholder
				</td>
				<td align="center"></td>
				</tr>
	<!-- 
	Track 2 data upload
	 -->	
	 			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 2:</strong>
					</div></td>
				<td nowrap="nowrap"><select name='trackType2' id='trackType2' onchange="Javascript: trackDownload('2'); connectPlotType('2');">
						<option value="NONE" >No track</option>
						<option value="Heatmap">Heatmap</option>
						<option value="Histogram" selected='selected'>Histogram</option>
						<option value="Line">Line Plot</option>
						<option value="Scatter">Scatter Plot</option>
						<option value="Tile">Tile Plot</option>
				</select>
				<a id='track2_a' href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#Histogram')">?</a>
				</td>
				
				<td name="ph2">placeholder</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>	
	<!-- 
	Track 3 data upload
	 -->
	 	 			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 3:</strong>
					</div></td>
				<td nowrap="nowrap"><select name='trackType3' id='trackType3' onchange = "Javascript: trackDownload('3'); connectPlotType('3');">
						<option value="NONE" >No track</option>
						<option value="Heatmap">Heatmap</option>
						<option value="Histogram">Histogram</option>
						<option value="Line" selected='selected'>Line Plot</option>
						<option value="Scatter">Scatter Plot</option>
						<option value="Tile">Tile Plot</option>
				</select>
				<a id='track3_a' href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#Line')">?</a>
				</td>
				<td name="ph3">placeholder</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>	
			
			<!-- Track 4 data upload -->
			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 4:</strong>
					</div></td>
				<td nowrap="nowrap"><select name='trackType4' id='trackType4' onchange="Javascript: trackDownload('4'); connectPlotType('4');">
						<option value="NONE" >No track</option>
						<option value="Heatmap">Heatmap</option>
						<option value="Histogram">Histogram</option>
						<option value="Line">Line Plot</option>
						<option value="Scatter" selected ='selected'>Scatter Plot</option>
						<option value="Tile">Tile Plot</option>
				</select>
				<a id='track4_a' href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#Scatter')">?</a>
				</td>
				<td name="ph4">placeholder</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<!-- Track 5 data upload -->
			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 5:</strong>
					</div></td>
				<td nowrap="nowrap"><select name='trackType5' id='trackType5' onchange="Javascript: trackDownload('5'); connectPlotType('5');">
						<option value="NONE" >No track</option>
						<option value="Heatmap" >Heatmap</option>
						<option value="Histogram">Histogram</option>
						<option value="Line">Line Plot</option>
						<option value="Scatter">Scatter Plot</option>
						<option value="Tile" selected ='selected'>Tile Plot</option>
				</select>
				<a id='track5_a' href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#Tile')">?</a>
				</td>
				<td name="ph5">placeholder</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<!-- 
			Dimensions
			 -->

			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Width/Height(in):</strong>
					</div>
				</td>
				<td><input name="width" type="text" id="width" value=6><br>
				</td>
				<td><div align="left">
						<strong>Track Width:</strong> (*chr width) <input
							name="trackHeight" type="text" id="trackHeight" value=1 onchange="Javascript: fixHeight()"><br>
					</div>
				</td>
				<td id='history'></td>
			</tr>
			<!-- 
	Link lines data upload
	 -->
			<tr>
				<td>&nbsp;</td>
				<td><a title='Providing your email address allows us to send a download link of your plot directly to you once it is done. It is never stored on the server and cannot be used for any other purpose.'><strong>Optional Email Address:</strong></a>
				<input id='email' name='email' type='text'></input>
				<a id='email_a' href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#email')">?</a></td>
				<td><strong>File Type:</strong><select name='fileType' id='fileType'>
						<option value="png" >png</option>
						<option value="jpeg" >jpeg</option>
						<option value="pdf">pdf</option>
				</select>
				 </td>
				<td><a
					onclick="Javascript:  var dis=document.getElementById('chrInclude'); im = document.getElementById('img_1'); if(dis.style.display == 'block'){ dis.style.display = 'none'; im.src='images/expand.gif'; } else { dis.style.display = 'block'; im.src='images/collapse.gif';};"
					href="Javascript: void(0);"> Chr: Include <img id="img_1"
						width="13" hspace="5" height="13" border="0" align="absmiddle"
						name="img_1" src="images/expand.gif"></img>
				</a>
					<div id="chrInclude"
						style="display: none; position: relative; left: 20px; border: thin none; background-color: rgb(238, 238, 238);">
						<table>
							<tbody>
								<tr>
									<td><input id="chrList[]" type="checkbox" value="chr1"
										name="chrList[]" checked></input> Chr1</td>
									<td><input id="chrList[]" type="checkbox" value="chr2"
										name="chrList[]" checked></input> Chr2</td>
									<td><input id="chrList[]" type="checkbox" value="chr3"
										name="chrList[]" checked></input> Chr3</td>
									<td><input id="chrList[]" type="checkbox" value="chr4"
										name="chrList[]" checked></input> Chr4</td>
									<td><input id="chrList[]" type="checkbox" value="chr5"
										name="chrList[]" checked></input> Chr5</td>
									<td><input id="chrList[]" type="checkbox" value="chr6"
										name="chrList[]" checked></input> Chr6</td>
									<td><input id="chrList[]" type="checkbox" value="chr7"
										name="chrList[]" checked></input> Chr7</td>
									<td><input id="chrList[]" type="checkbox" value="chr8"
										name="chrList[]" checked></input> Chr8</td>
								</tr>
								<tr>
									<td><input id="chrList[]" type="checkbox" value="chr9"
										name="chrList[]" checked></input> Chr9</td>
									<td><input id="chrList[]" type="checkbox" value="chr10"
										name="chrList[]" checked></input> Chr10</td>
									<td><input id="chrList[]" type="checkbox" value="chr11"
										name="chrList[]" checked></input> Chr11</td>
									<td><input id="chrList[]" type="checkbox" value="chr12"
										name="chrList[]" checked></input> Chr12</td>
									<td><input id="chrList[]" type="checkbox" value="chr13"
										name="chrList[]" checked></input> Chr13</td>
									<td><input id="chrList[]" type="checkbox" value="chr14"
										name="chrList[]" checked></input> Chr14</td>
									<td><input id="chrList[]" type="checkbox" value="chr15"
										name="chrList[]" checked></input> Chr15</td>
									<td><input id="chrList[]" type="checkbox" value="chr16"
										name="chrList[]" checked></input> Chr16</td>
								</tr>
								<tr>
									<td><input id="chrList[]" type="checkbox" value="chr17"
										name="chrList[]" checked></input> Chr17</td>
									<td><input id="chrList[]" type="checkbox" value="chr18"
										name="chrList[]" checked></input> Chr18</td>
									<td><input id="chrList[]" type="checkbox" value="chr19"
										name="chrList[]" checked></input> Chr19</td>
									<td id="Chr20"><input id="chrList[]" type="checkbox"
										value="chr20" name="chrList[]" checked></input> Chr20</td>
									<td id="Chr21"><input id="chrList[]" type="checkbox"
										value="chr21" name="chrList[]" checked></input> Chr21</td>
									<td id="Chr22"><input id="chrList[]" type="checkbox"
										value="chr22" name="chrList[]" checked></input> Chr22</td>
									<td><input id="chrList[]" type="checkbox" value="chrX"
										name="chrList[]" checked></input> ChrX</td>
									<td id="ChrY"><input id="chrList[]" type="checkbox"
										value="chrY" name="chrList[]" checked></input> ChrY</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>

				<td><div align="left">
						<strong>Plot name:</strong>(without file extension)<input name="plotName" type="text" id="plotName" onchange="Javascript: fixName();"><br>
					</div></td>
				<td>&nbsp;</td>
			</tr>
			<tr> <td colspan="6" align="center">
			<INPUT type="Submit" Name=Submit VALUE="Plot Data" id="mySubmit">
          <input type="reset" value="Reset" onclick="Javascript: speciesReset()"/>  <a onclick="Javascript:document.getElementById('circos_div').innerHTML = '<img src=\'images/spinner2-greenie.gif\'   style=\'position:relative; left:0px;\' height=\'30\' width=\'30\' />'; window.setTimeout(function() { document.getElementById('circos_div').innerHTML = '<img src=\'images/example.png\' height=\'800\' width=\'800\' />'}, 15000);">Run Demo</a>  | 
          
          <a onclick="Javascript:$('#tabs').tabs('option', 'active', 0);">Upload Files</a> 
          
              </td>
</tr>
</table>

</form>
</div>
<div id='circos_div' align='center'></div>
</div>
</td></tr></table>
<div id='stop_div'><a style="display:none">huehuehue</a></div>
<div id='modal' style="display:none">We have begun generating your plot. If you provided an email we will send you a notification when it is complete. Please wait until this has finished before starting another one.</div>
<div id='modal2' style="display:none">We have begun generating your plot. If you provided an email we will send you a notification when it is complete. Please wait until this has finished before starting another one.</div>
<div align="center">
Questions? Comments? Contact peter.hansen.nih@gmail.com
</div>
</body>
</html>